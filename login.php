<?php
  session_start();
  if($_SESSION['login'] == true){
    echo "<script>window.location.href='index.php';</script>";
  }else if(isset($_POST['username']) && isset($_POST['password'])){
  	$db = mysqli_connect("localhost", "root", "rahasia", "pakan");
		$user = mysqli_query($db, "SELECT * FROM user WHERE username='".$_POST['username']."' AND password=MD5('".$_POST['password']."')");
		if($user->num_rows == 1){
			$data = mysqli_fetch_array($user,MYSQLI_ASSOC);
			$_SESSION['login'] = true;
			$_SESSION['username'] = $_POST['username'];
			$_SESSION['hak_akses'] = $data['hak_akses'];
      
      echo "<script>window.location.href='index.php';</script>";
		}
		mysqli_close();
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>e-Pakan</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
  </head>

  <body style="background:#F7F7F7;">
    <div class="">
      <div id="wrapper">
        <div id="login" class=" form">
          <section class="login_content">
            <form method="post">
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" name="username" placeholder="Username" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="Password" required="" />
              </div>
              <div>
                <button type="submit" class="btn btn-default">Login</button>
              </div>
              <div class="clearfix"></div>
              <div class="separator">
                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-wifi" style="font-size: 26px;"></i> e-Pakan</h1>
                  <p>Best Chicken Autofeeder</p>
                  <p>© 2016 All Rights Reserved</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>